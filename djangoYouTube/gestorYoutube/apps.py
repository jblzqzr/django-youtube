from django.apps import AppConfig

class GestoryoutubeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gestorYoutube'

