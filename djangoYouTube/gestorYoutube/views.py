from django.shortcuts import render
from django.http import HttpResponse
from .models import Videos
import urllib.request
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from django.views.decorators.csrf import csrf_exempt


class CounterHandler(ContentHandler):
    def __init__(self):
        self.inEntry = False
        self.inVideo = False
        self.title = ""
        self.theVideo = ""
        self.url = ""
        self.id = ""
        self.videos = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inVideo = True
            elif name == 'link':
                self.url = attrs.get('href')
            elif name == 'yt:videoId':
                self.inVideo = True

    def endElement(self, name):
        if name == 'entry':
            self.inEntry = False
            if not Videos.objects.filter(url=self.url).exists():
                c = Videos.objects.create(title=self.title, url=self.url, id=self.id)
                c.save()
            self.videos = self.videos \
                          + "    <li><a href='" + self.url + "'>" \
                          + self.title + "</a></li>\n"

        elif self.inEntry:
            if name == 'title':
                self.title = self.theVideo
                self.theVideo = ""
                self.inVideo = False
            elif name == 'yt:videoId':
                self.id = self.theVideo
                self.theVideo = ""
                self.inVideo = False

    def characters(self, chars):
        if self.inVideo:
            self.theVideo = self.theVideo + chars


def init_database():
    url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'
    xmlStream = urllib.request.urlopen(url)
    YtParser = make_parser()
    YtHandler = CounterHandler()
    YtParser.setContentHandler(YtHandler)

    # Ready, set, go!
    xmlFile = urllib.request.urlopen("https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg")
    YtParser.parse(xmlFile)


# Create your views here.
@csrf_exempt
def index(request):
    if request.method == 'GET':
        init_database()

    elif request.method == 'POST':
        body = request.POST['selected']
        idVideo = request.POST['id']
        if body == 'true':
            c = Videos.objects.get(id=idVideo)
            c.selected = True
            c.save()
        else:
            c = Videos.objects.get(id=idVideo)
            c.selected = False
            c.save()
    non_selected_list = Videos.objects.filter(selected=False)
    selected_list = Videos.objects.filter(selected=True)
    return render(request, 'gestorYoutube/index.html', {'non_selected_list':non_selected_list, 'selected_list':selected_list})